import sympy as sp
from astropy import units as u

Vcomp = 1.18*u.V
Rs    = 20  *u.kOhm
Iconst= 1.5 *u.uA
Iadd  = 4   *u.uA

#from threshold values calculate resistors
Von   = 14*u.V
Voff  = 12*u.V
Rtop  = (0.915 * Von - Voff) / (4.127 * u.uA)
Rbot  = 1.08 * u.V * Rtop / (Voff - 1.08 * u.V + 5.5 *u.uA * Rtop)
print(f'calculated resistor values Rbot {Rbot.to(u.kOhm)}, Rtop {Rtop.to(u.kOhm)}')

#from resistors calculate threshold values
#Rtop  = 100 *u.kOhm
#Rbot  = 10  *u.kOhm

ratio = Rbot/(Rtop + Rbot)
Reff  = Rtop*Rbot/(Rtop + Rbot)

Voff  = (Vcomp - Iconst * (Rs + Reff))          / ratio
Von   = (Vcomp - (Iconst + Iadd) * (Rs + Reff)) / ratio

print(f'EN thresholds {Von} - {Voff}')

#feedback resistors
Rbot = 10 * u.kOhm
Vout = 12 * u.V
Rtop = Rbot * (Vout/0.8/u.V - 1)

print(f'feedback resistors for {Vout} are Rtop {Rtop}, Rbot {Rbot}')
