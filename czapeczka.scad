$fn=30;

module round_cube(size,radius)
{
    hull()
    {
        for(x=[-0.5,0.5])
        for(y=[-0.5,0.5])
        for(z=[-0.5,0.5])
            translate([(size[0]-2*radius)*x,(size[1]-2*radius)*y,(size[2]-2*radius)*z])
        sphere(r=radius);
    }
}

for(i=[-2:2])
translate([9.5*i,0,0])
{
    translate([0,0,-1.6])
    cylinder(d=2,h=1.6);
    translate([0,0,5/2])
    cube([3,7,5],true);
}
translate([0,7/2-1.5/2,2.5])
cube([41,1.5,5],true);
translate([0,0,5+3/2])
cube([41,7,3],true);