import matplotlib.pyplot as plt
import numpy as np

I_pos=2.2 #[A]
I_neg=0.3 #[A]
V_in=15 #[V]

V_f    =0.5   #[V]
R_d    =0.039 #[R]
FB_R   =0.010 #[R]
R_low  =0.032 #[R]
R_high =0.067 #[R]
R_L    =0.092 #[R]
L_L    =22    #[uH]
frequency=1.2 #[MHz]
I_sat  =4.7   #[A]

#output ferrite bead resistive losses
FB_pos_P=I_pos**2*FB_R
FB_neg_P=I_neg**2*FB_R
print('output ferrites dissipate: ',FB_pos_P,FB_neg_P,' W')
#SMPS math
duty_cycle=(12+V_f)/V_in
ΔI=duty_cycle*12/(L_L*frequency)
print('ripple current: ',ΔI,' A')


#negative converter losses
I_neg_peak=I_neg/(1-duty_cycle)
I_neg_RMS=I_neg_peak*np.sqrt(1-duty_cycle)
D_neg_P=V_f*I_neg_RMS+R_d*I_neg_RMS**2
L_neg_P=R_L*I_neg_RMS**2
#positive converter losses
I_5=I_pos-I_neg-ΔI*(3/2-duty_cycle)
print('peak inductor current: ',I_5+ΔI+I_neg_peak,' A')
assert I_5+ΔI+I_neg_peak<I_sat , "inductor saturated!"
#I_mean=I_5+I_neg/(1-duty_cycle)*duty_cycle+ΔI/2
I_pos_RMS=1/12*ΔI+np.sqrt(I_5**2*(1-duty_cycle)+(I_5+I_neg_peak)**2*duty_cycle)
L_pos_P=R_L*I_pos_RMS**2

#SMPS IC losses
#conduction loss
P_con=(I_5+ΔI/2)**2*R_low*(1-duty_cycle)+(I_5+I_neg_peak+ΔI/2)**2*R_low*duty_cycle
print('SMPS conduction loss: ',P_con,' W')


print('positive/negative inductor loss: ',L_pos_P,L_neg_P,' W')
print('total inductor loss: ',L_pos_P+L_neg_P,' W')
print('diode loss: ',D_neg_P,' W')
P_tot=FB_pos_P+FB_neg_P+P_con+L_pos_P+L_neg_P+D_neg_P
I_in=(P_tot+12*(I_neg+I_pos))/V_in
print('input current: ',I_in)
print('input FB loss: ',I_in**2*FB_R,' W')
P_tot=FB_pos_P+FB_neg_P+P_con+L_pos_P+L_neg_P+D_neg_P+I_in**2*FB_R
print('total power dissipated: ',P_tot,' W')
print('efficiency: ',(1-P_tot/(P_tot+12*(I_neg+I_pos)))*100,' %')
